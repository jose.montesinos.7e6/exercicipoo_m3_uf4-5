/*
4- Quiz

Volem fer un petit programa de preguntes.
Tenim dos tipus de preguntes:
opció múltiple
text lliure

Necessitaràs les següents classes
Question
FreeTextQuestion
MultipleChoiseQuestion
Quiz (conté una llista de preguntes)

Pas 1: Fes l'estructura de dades que et permeti guardar aquestes preguntes.
Pas 2: Fes un programa que, donat un quiz creat per codi, imprimeixi les preguntes.
Pas 3: Permet que l'usuari pugui respondre les preguntes.
Pas 4: Un cop acabat, imprimeix quantes respostes són correctes.
*/

abstract class Question(var pregunta: String, var resposta: String, var respostaUsuari: String){
    abstract fun esCorrecte():Boolean
}

class FreeTestQuestion(pregunta:String, resposta: String, respostaUsuari: String):Question(pregunta, resposta, respostaUsuari){
    override fun toString ():String{
        return(pregunta)
    }
    override fun esCorrecte():Boolean{
        var correcte:Boolean = false
        if(super.resposta.lowercase()==super.respostaUsuari.lowercase()) correcte=true //println("Es correcte")
        //else{println("Aquesta no es la resposta")}
        return correcte
    }
}

class multipleChoiseQuestion(pregunta:String, resposta:String, respostaUsuari:String, var opcions: Array<String>):Question(pregunta, resposta, respostaUsuari){
    override fun toString():String{
        return("${pregunta}\n" +
                "1) ${opcions[0]}\n" +
                "2) ${opcions[1]}\n" +
                "3) ${opcions[2]}\n")
    }
    override fun esCorrecte():Boolean{
        var correcte = false
        if(super.resposta.lowercase() == super.respostaUsuari.lowercase()) correcte = true//println("Es correcte!")
        //else println("Aquesta no era la opció correcte")
        return correcte
    }
}

class Quiz(){
    var llistaPreguntes = mutableListOf<Question>()

    fun afegirPregunta(pregunta:Question){
        llistaPreguntes.add(pregunta)
    }

    fun mostrarPregunta(i:Int){
        println(llistaPreguntes[i])
    }

    fun calcularPuntuacio():Int{
        var puntuacio = 0
        for(i in 0..llistaPreguntes.size-1){
            if(llistaPreguntes[i].esCorrecte()){
                puntuacio++
            }
        }
        return puntuacio
    }
}
fun main(){
    val preguntes = Quiz()
    val Q1 = FreeTestQuestion("Quin és l'animal més gran del món?","La balena blava", "")
    val Q2 = multipleChoiseQuestion("En quin dels següents brazos de la vía làctea es troba el sistema solar","Sagitari","",
        arrayOf<String>("Centari","Sagitari","Exterior"))
    val Q3 = multipleChoiseQuestion("Com es diu el creador del llenguatge de rpogramació Kotlin?", "Andrey Breslav", "", arrayOf<String>("Andrey Breslav","Roberto Gamboa","Andriu Breslav"))
    val Q4 = multipleChoiseQuestion("El siici amb impurèses de fòsfor es un semiconductor de tipus:","tipus n","",arrayOf("tipus p","tipus n","tipus j"))
    preguntes.afegirPregunta(Q1)
    preguntes.afegirPregunta(Q2)
    preguntes.afegirPregunta(Q3)
    preguntes.afegirPregunta(Q4)

    for(i in 0..preguntes.llistaPreguntes.size-1){
        preguntes.mostrarPregunta(i)
        println("Escriu la teva resposta")
        var resposta = readln()
        preguntes.llistaPreguntes[i].respostaUsuari = resposta
        preguntes.llistaPreguntes[i].esCorrecte()
    }
    println("La teva puntuació es: ${preguntes.calcularPuntuacio()}/${preguntes.llistaPreguntes.size}")
}