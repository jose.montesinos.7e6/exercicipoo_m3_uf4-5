/*
3- InstrumentSimulator

Volem fer un petit programa d'emulació d'instruments.
Tenim els següents instruments.
Tambor
Triangle
Cada tambor té un to que pot ser A, O, U.
Els tambors amb A fan TAAAM, els tambors amb O fan TOOOM i els tambors amb U fan TUUUM.
Els triangles tenen una resonancia que va del 1 al 5.
Els triangles amb resonancia 1 fan TINC, els de dos TIINC, … i els de 5 TIIIIINC.
Fes les classes necessàries per poder executar el següent programa
 */

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}

abstract class Instrument(){
    abstract fun makeSounds(vegades:Int)
}

class Triangle(var durada:Int):Instrument(){
    override fun makeSounds(vegades:Int){
        var llargadaSo = "T"
        for(i in 1.. durada){
            llargadaSo = llargadaSo+"I"
        }
        llargadaSo = llargadaSo+"NC"
        println(llargadaSo)
    }
}

class Drump(var tipusSo: String):Instrument(){
    override fun makeSounds(vegades: Int){
        var sortida = "T"
        for(i in 1..3){
            sortida = sortida+this.tipusSo
        }
        sortida = sortida+"M"
        println(sortida)
    }
}

