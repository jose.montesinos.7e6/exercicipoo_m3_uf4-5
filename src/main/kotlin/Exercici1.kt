import java.io.File

/*
1- StudentWithTextGrade
Volem enmagatzemar d'un estudiant, el seu nom, i la seva nota en format text. Una nota pot ser suspès, aprovat, bé, notable o excel·lent.

Fes un programa que crei 2 estudiants amb dues notes diferents i printa'ls per pantalla

Output

Student{name='Mar', textGrade=FAILED}
Student{name='Joan', textGrade=EXCELLENT}
 */

class Alumne(var nom:String,var textGrade:String){
    fun mostrarAlumne(){
        if(this.textGrade=="EXCELLENT" || this.textGrade=="FAILED" || this.textGrade=="REMARKABLE" || this.textGrade=="GOOD" || this.textGrade=="ENOUGH"){
            println("Alumne: ${this.nom}, Nota: ${this.textGrade}")
        }else{
            println("Aquesta no es una nota, esccriu FAILED si està suspès, ENOUGH si està aprovat amb suficient, GOOD si te una nota bona, REMARKABLE asi ha tret un notable o EXCELLENT si la seva nota es excellent")
            //main()
        }

    }

    fun crearArxiu(){
        val Arxiu = File("./FitxersAlumnes/${this.nom}")
        Arxiu.createNewFile()
        Arxiu.writeText("Nom: ${this.nom}, Nota: ${this.textGrade}")
    }
}

fun main() {
    //Objecte alumne1 ja introduït
    val alumne1 = Alumne("Joan", "EXCELLENT")

    //Objecte alumne2 que instrodueïx l'usuari per pantalla
    println("Introdueix el nom de l'alumne")
    val nomAlumne2 = readln()
    println("Introdueix la nota de l'alumne, esccriu FAILED si està suspès, ENOUGH si està aprovat amb suficient, GOOD si te una nota bona, REMARKABLE asi ha tret un notable o EXCELLENT si la seva nota es excellent")
    val notaAlumne2 = readln()
    val alumne2 = Alumne(nomAlumne2, notaAlumne2)

    println(alumne1.mostrarAlumne())
    println(alumne2.mostrarAlumne())
    alumne1.crearArxiu()
    alumne2.crearArxiu()
}
