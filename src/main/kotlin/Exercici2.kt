/*
2- Rainbow
Fes un programa que llegeixi per pantalla un color i indica si forma part de l'arc de Sant Mart
 */

class arcSanMarti(var color:String){
    fun resposta() {
        when (this.color) {
            "vermell" -> println("Pertany al Arc de San Martí")
            "taronja" -> println("Pertany al Arc de San Martí")
            "groc" -> println("Pertany al Arc de San Martí")
            "verd" -> println("Pertany al Arc de San Martí")
            "blau" -> println("Pertany al Arc de San Martí")
            "anyil" -> println("Pertany al Arc de San Martí")
            "violeta" -> println("Pertany al Arc de San Martí")
            else -> println("No pertany al arc de San Martí")
        }
    }
}

fun main(){
    println("Introdueix el color i et diré si pertany al arc de San Martí")
    val color = readln().lowercase()
    val pertanyAlArc = arcSanMarti(color)

    pertanyAlArc.resposta()
    //main()
}