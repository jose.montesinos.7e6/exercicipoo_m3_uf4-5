import java.util.*

/*
5- GymControlApp
Volem fer un programa pel control d'entrada d'un club esportiu. El programa ha de ser generic per tots els clubs, però tenim clubs que utilitzen un dispositiu de lectura dactilar, uns altres fan servir una targeta electrònica, uns altres enregistren l'entrada manualment… Tots els gimnasos tenen un únic punt d'entrada i per tant, el primer cop que es registra un identificador indica que l'usuari ha entrat, i el segón cop indica que l'usuari ha sortit.
Per tal de resoldre-ho, hem pactat una interfcície comuna anomenada GymControlReader que ens retorna l'identificador del següent usuari registrat:
fun nextId() : String
A més a més, ens donen la implementació següent que és la utilitzada pel sistema de lectura manual
class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}
Crea un programa que, donat un GymControlReader, llegeixi 8 identificadors. Imprimeix per pantalla si l'usuari ha entrat o ha sortit. Utilitza la implementació donada.

Input
1548
8768
4874
1548
1354
1548
3586
1354
Output
1548 Entrada
8768 Entrada
4874 Entrada
1548 Sortida
1354 Entrada
1548 Entrada
3586 Entrada
1354 Sortida
 */
interface GymControlReader{
    fun nextId():String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    var registres = mutableListOf<String>()
    override fun nextId(): String = scanner.next()
    fun agregarRegistro(user:String){
        if(user in this.registres) {registres.remove(user);println("$user Sortida")}
        else {this.registres.add(user);println("$user Entrada")}
    }
}

fun main(){
    val gmr = GymControlManualReader()
    for(i in 0..8){
        gmr.agregarRegistro(gmr.nextId())
    }
}